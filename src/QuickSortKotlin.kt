fun main() {
    val arr = intArrayOf(10, 7, 8, 9, 1, 5)

    println("Saralanish kerak massiv")
    printArray(arr)
    /**
     * n massiv o'lchami
     */
    val n = arr.size
    sort(arr, 0, n - 1)

    println("Saralangan massiv")
    printArray(arr)
}

fun partition(arr: IntArray, startIndex: Int, endIndex: Int): Int {
    /**
     * Massivda tayanch elementni olish.
     */
    val pivot = arr[endIndex]
    /**
     *  Tayanch elementdan kichik bo'lgan elementlar
     *  uchun indexning boshlanishi
     */
    var i = startIndex - 1

    for (j in startIndex until endIndex) {
        /**
         *  Massivning j indexdagi elementi tayanch
         *  elementdan kichik bo'lsa chap tomonga to'plash
         */
        if (arr[j] < pivot) {
            i++
            /**
             * Massivning i va j indexdagi
             * elementlarni o'rnini almashtirish
             */
            val temp = arr[i]
            arr[i] = arr[j]
            arr[j] = temp
        }
    }
    /**
     * i indexgacha tayanch elementdan kichik
     * bo'lgan elementlar joylandi endi
     * tayanch elementni massivning oxiridan [i + 1]
     * indexga joylaymiz
     */
    val temp = arr[i + 1]
    arr[i + 1] = arr[endIndex]
    arr[endIndex] = temp
    return i + 1
}

fun sort(arr: IntArray, startIndex: Int, endIndex: Int) {
    /**
     * Massivni bo'laklarga bo'lish toki har
     * bir element yakka qolgunicha davom
     * etadi va saralash o'zi nihoyasiga yetadi
     */
    if (startIndex < endIndex) {
        /**
         * pi - bu tayanch elementning indexi
         */
        val pi = partition(arr, startIndex, endIndex)

        /**
         * pi - indexgacha bo'lgan massivning alohida
         * massiv sifati qarab saralash
         */
        sort(arr, startIndex, pi - 1)

        /**
         * pi - indexdan keyingi massivning alohida
         * massiv sifati qarab saralash
         */
        sort(arr, pi + 1, endIndex)
    }
}

/**
 * Massivni chop etish uchun
 */
fun printArray(arr: IntArray) {
    val n = arr.size
    for (i in 0 until n) print(arr[i].toString() + " ")
    println()
}
